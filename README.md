# Deploying a ROS2 demo application

A simple demo to build, package and deploy a two-node ROS application packaged as two services, one talker and one listener.

This demo is built using the [https://hub.docker.com/_/ros/](official ros2 images) with the talker-listener demo application from the [https://github.com/ros2/demos](public ros2 demo repository).

## Build and push the containers

``` shell
docker build -t registry.trial.cto.avassa.net/ros2-demo-talker -f Dockerfile.talker
docker build -t registry.trial.cto.avassa.net/ros2-demo-listener -f Dockerfile.listener
```

Push the container image to your control tower instance, but **don't forget** to change the Control Tower URL to your own below:

``` shell
docker push registry.trial.cto.avassa.net/ros2-demo-talker
docker push registry.trial.cto.avassa.net/ros2-demo-listener
```

## Deploy and inspect the application

Create a new application and configure it according to the `ros2-demo.app.yml`:

```yaml
::include{file=ros2-demo.app.yml}
```

Deploy the new application to some set of hosts by creating a deployment specification.

Inspect an application instance in any of the sites and look at the logs of both `talker` and `listener` to confirm that they are able to talk to each other. The `listener` should log something like:

```text
[
  "[INFO] [1738241134.442462751] [listener]: I heard: [Hello World: 1420]",
  "[INFO] [1738241135.442436730] [listener]: I heard: [Hello World: 1421]",
  "[INFO] [1738241136.442500859] [listener]: I heard: [Hello World: 1422]",
  "[INFO] [1738241137.442465969] [listener]: I heard: [Hello World: 1423]",
  "[INFO] [1738241138.442563575] [listener]: I heard: [Hello World: 1424]"
]
```
